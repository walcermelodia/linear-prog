//
// Created by walce on 15.11.2022.
//

#include "lab3.h"
#include <iostream>


lab3::matrix lab3::transpose(const lab3::matrix &m) {
    matrix res(m[0].size(), std::vector<double>(m.size()));

    for (size_t i = 0; i < m.size(); ++i) {
        for (size_t j = 0; j < m[0].size(); ++j) {
            res[j][i] = m[i][j];
        }
    }

    return res;
}

std::tuple<lab3::matrix, lab3::vector, lab3::vector> lab3::reduction_canon_form_and_create_basis(
        const lab3::matrix &m,
        const lab3::vector &c,
        const lab3::constraints &b) {
    const size_t N = b.size();

    matrix m_ = transpose(m);
    vector c_ = c;
    vector b_(N);

    for (size_t i = 0; i < N; ++i) {
        b_[i] = b[i].second;
        vector tmp(N);
        tmp[i] = 1;
        m_.push_back(tmp);

        if (lab3::LESS_THAN_OR_EQUAL == b[i].first) {
            c_.push_back(0.0);
        } else {
            c_.push_back(-M);
        }
    }

    return {m_, c_, b_};
}

void lab3::calc_weights(const lab3::matrix &A, const lab3::vector &c_b, const lab3::vector &c, lab3::vector &weights) {
    for (size_t i = 0; i < weights.size(); ++i) {
        weights[i] = std::inner_product(c_b.begin(), c_b.end(), A[i].begin(), 0.0) - c[i];
    }
}

std::optional<lab3::vector> lab3::artificial_variable_basis_method(const lab3::matrix &A,
                                                                   const lab3::vector &c,
                                                                   const lab3::constraints &b) {
    auto[m_, c_, b_] = reduction_canon_form_and_create_basis(A, c, b);

    const size_t N = b.size();
    std::vector<std::pair<std::vector<double>, size_t>> basis;
    lab3::vector c_b;
    for (size_t i = m_.size() - N; i < m_.size(); ++i) {
        basis.emplace_back(m_[i], i);
        c_b.push_back(c_[i]);
    }

    lab3::vector weights(m_.size());
    size_t row, col;
    double func_value_prev = std::inner_product(c_b.begin(), c_b.end(), b_.begin(), 0.0);
    double func_value_cur;
    while (true) {
        calc_weights(m_, c_b, c_, weights);

        bool is_all_weights_not_negative = std::all_of(weights.begin(), weights.end(), [](auto elem){return elem >= 0;});
        if (is_all_weights_not_negative) {
            return lab3::get_result(basis, b_, c.size());
        }

        col = std::max_element(weights.begin(), weights.end(), [](auto a, auto b){
            return b < 0.0 && std::abs(a) < std::abs(b);}) - weights.begin();

        bool is_all_elem_lead_col_not_positive = std::all_of(m_[col].begin(), m_[col].end(), [](auto elem){return elem <= 0.0;});
        if (is_all_elem_lead_col_not_positive) {
            return std::nullopt;
        }

        row = lab3::find_row(b_, m_[col]);

        size_t from = col;
        size_t to   = basis[row].second;
        lab3::swap_basis_vector(m_, b_, from, to);
        basis[row] = {m_[from], from};
        c_b[row] = c_[col];

        func_value_cur = std::inner_product(c_b.begin(), c_b.end(), b_.begin(), 0.0);


        if (func_value_cur < func_value_prev) {
            throw std::runtime_error("the function is decreasing");
        }

        func_value_prev = func_value_cur;
    }
}

std::optional<lab3::vector> lab3::get_result(const std::vector<std::pair<std::vector<double>, size_t>> &basis, const lab3::vector &b,
                                             size_t dimension) {
    vector res(dimension);

    for (size_t i = 0; i < basis.size(); ++i) {
        res[basis[i].second] = b[i];
    }

    return std::optional<lab3::vector>{res};
}

size_t lab3::find_row(const lab3::vector &a, const lab3::vector &b) {
    size_t pos_first_positive_b = std::find_if(b.begin(), b.end(), [](auto elem){return elem > 0;}) - b.begin();

    double min_elem = a[pos_first_positive_b] / b[pos_first_positive_b];

    size_t pos = pos_first_positive_b;
    for (size_t i = pos_first_positive_b + 1; i < b.size(); ++i) {
        if (b[i] <= 0.0) continue;

        double tmp = a[i] / b[i];

        if (tmp < min_elem) {
            min_elem = tmp;
            pos = i;
        }
    }

    return pos;
}

void lab3::swap_basis_vector(lab3::matrix &m, lab3::vector &b, size_t from, size_t to) {
    size_t row_pos_target_elem = std::find_if(m[to].begin(), m[to].end(), [](auto elem){return elem == 1.0;}) - m[to].begin();

    double target_elem = m[from][row_pos_target_elem];

    for (auto & col : m) {
        col[row_pos_target_elem] /= target_elem;
    }

    b[row_pos_target_elem] /= target_elem;

    for (size_t row = 0; row < m[0].size(); ++row) {
        if (row == row_pos_target_elem) continue;

        double multiplier = m[from][row];
        for (auto & col : m) {
            double row_target_elem = col[row_pos_target_elem];
            col[row] -= multiplier * row_target_elem;
        }

        b[row] -= b[row_pos_target_elem] * multiplier;
    }
}

