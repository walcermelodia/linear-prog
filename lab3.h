//
// Created by walce on 15.11.2022.
//

#ifndef LINEAR_PROG_LAB3_H
#define LINEAR_PROG_LAB3_H

#include <vector>
#include <tuple>
#include <numeric>
#include <algorithm>
#include <optional>

namespace lab3 {
    enum limit {
        LESS_THAN_OR_EQUAL, EQUALS
    };

    const double M = 1e5;

    using vector      = std::vector<double>;
    using matrix      = std::vector<std::vector<double>>;
    using constraints = std::vector<std::pair<limit, double>>;

    matrix transpose(const matrix &m);
    std::tuple<matrix, vector, vector> reduction_canon_form_and_create_basis(const matrix &m,
                                                                             const vector &c,
                                                                             const constraints &b);
    std::optional<vector> artificial_variable_basis_method(const matrix &A, const vector &c, const constraints &b);
    void calc_weights(const matrix &A, const vector &c_b, const vector &c, vector &weights);
    std::optional<vector> get_result(const std::vector<std::pair<std::vector<double>, size_t>> &basis,
                                     const vector &b, size_t dimension);
    size_t find_row(const vector &a, const vector &b);
    void swap_basis_vector(matrix &m, vector &b, size_t from, size_t to);
}

#endif //LINEAR_PROG_LAB3_H
