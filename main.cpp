#include <iostream>
#include "lab3.h"
#include <functional>
#include <sstream>
#include <string>

std::string print_result(const lab3::vector &v) {
    std::ostringstream  ss;

    for (auto elem : v) {
        ss << elem << " ";
    }
    return ss.str();
}

void TEST_1() {
    lab3::vector c = {2, 1, -1, 1, -1};
    lab3::matrix m = { {1, 1, 1, 0, 0},
                       {2, 1, 0, 1, 0},
                       {1, 2, 0, 0, 1}};
    lab3::constraints b = { {lab3::EQUALS, 5},
                            {lab3::EQUALS, 9},
                            {lab3::EQUALS, 7}};
    auto res = lab3::artificial_variable_basis_method(m, c, b);
    if (*res == lab3::vector{3, 2, 0, 1, 0}) {
        std::cout << "TEST_PASSED: " << print_result(*res) << std::endl;
    } else {
        std::cout << "TEST_FAILED" << print_result(*res) << std::endl;
    }
}

void TEST_2() {
    lab3::vector c = {2, -1};
    lab3::matrix m = { {-1, 1},
                       {3, -5}};
    lab3::constraints b = { {lab3::LESS_THAN_OR_EQUAL, 1},
                            {lab3::LESS_THAN_OR_EQUAL, 6}};
    auto res = lab3::artificial_variable_basis_method(m, c, b);
    if(res == std::nullopt) {
        std::cout << "TEST_PASSED: no solution" << std::endl;
    } else {
        std::cout << "TEST_FAILED" << print_result(*res) << std::endl;
    }
}

void TEST_3() {
    lab3::vector c = {3, 0, 2, 0, 0, -6};
    lab3::matrix m = { {2,  1, -3, 0, 0,  6},
                       {-3, 0,  2, 1, 0, -2},
                       { 1, 0,  3, 0, 1, -4}};
    lab3::constraints b = { {lab3::EQUALS, 18},
                            {lab3::EQUALS, 24},
                            {lab3::EQUALS, 36}};
    auto res = lab3::artificial_variable_basis_method(m, c, b);
    if (*res == lab3::vector{18, 0, 6, 66, 0, 0}) {
        std::cout << "TEST_PASSED: " << print_result(*res) << std::endl;
    } else {
        std::cout << "TEST_FAILED" << print_result(*res) << std::endl;
    }
}

void TEST_4() {
    lab3::vector c = {6, 5};
    lab3::matrix m = { {-3, 5},
                       {-2, 5},
                       { 1, 0},
                       { 3, -8}};
    lab3::constraints b = { {lab3::LESS_THAN_OR_EQUAL, 25},
                            {lab3::LESS_THAN_OR_EQUAL, 30},
                            {lab3::LESS_THAN_OR_EQUAL, 10},
                            {lab3::LESS_THAN_OR_EQUAL,  6}};
    auto res = lab3::artificial_variable_basis_method(m, c, b);
    if (*res == lab3::vector{10, 10}) {
        std::cout << "TEST_PASSED: " << print_result(*res) << std::endl;
    } else {
        std::cout << "TEST_FAILED" << print_result(*res) << std::endl;
    }
}

void TEST_5() {
    lab3::vector c = {2, 1, -1};
    lab3::matrix m = { {1, 2,  1},
                       {3, 1, -1}};
    lab3::constraints b = { {lab3::LESS_THAN_OR_EQUAL, 3},
                            {lab3::EQUALS, 3}};
    auto res = lab3::artificial_variable_basis_method(m, c, b);
    if (*res == lab3::vector{0.60, 1.2, 0}) {
        std::cout << "TEST_PASSED: " << print_result(*res) << std::endl;
    } else {
        std::cout << "TEST_FAILED: " << print_result(*res) << std::endl;
    }
}

int main() {
    std::vector<std::function<void()>> TESTS = {TEST_1, TEST_2, TEST_3, TEST_4, TEST_5};

    for (const auto& test : TESTS) {
        test();
    }

    return 0;
}
